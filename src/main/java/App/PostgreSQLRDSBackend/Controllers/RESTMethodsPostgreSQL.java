package App.PostgreSQLRDSBackend.Controllers;

import App.CartoDBBackend.Dependencies.CartoDB.CartoDBException;
import App.CartoDBBackend.Dependencies.CartoDB.impl.SecuredCartoDBClient;
import App.CartoDBBackend.POJO.RequestClasses.PushRequestClass;

import App.PostgreSQLRDSBackend.POJO.RequestClasses.FriendInput;
import App.PostgreSQLRDSBackend.POJO.RequestClasses.ProfileRequestClass;
import App.PostgreSQLRDSBackend.POJO.RequestClasses.UserSettingsRequestClass;
import App.PostgreSQLRDSBackend.POJO.ResponseClasses.FollowResponseClass;
import App.PostgreSQLRDSBackend.POJO.ResponseClasses.FriendResponseClass;
import App.PostgreSQLRDSBackend.POJO.ResponseClasses.ProfileResponseClass;
import App.PostgreSQLRDSBackend.POJO.ResponseClasses.ServerResponse;
import App.PostgreSQLRDSBackend.Utils.CommonItems;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

@RestController
public class RESTMethodsPostgreSQL {

    private static final int QUERY_GOOD = 2;
    private static final int QUERY_BAD = -2;

    private final static int PHOTO_UPLOAD_OR_NO_CHANGE = 1;
    private final static int USERNAME_IN_USE = 2;
    private final static int PHOTO_AND_OR_USERNAME_UPDATED = 3;
    private final static int NEW_USER_ADDED = 4;

    @RequestMapping(value = "/fetch_followers", method = RequestMethod.POST)
    public ResponseEntity<ArrayList<FriendResponseClass>> fetch_followers(@RequestBody String input) {

        String userID;
        Connection database;
        ArrayList<FriendResponseClass> response = new ArrayList<>();

        if (input != null) {
            userID = input;
            database = CommonItems.getConnection();

            try {

                String sql = "SELECT alias, profile_picture, public.profile.user_id\n" +
                        "FROM public.profile \n" +
                        "JOIN public.friends on profile.user_id = friends.user_id\n" +
                        "WHERE friends.friend_user_id =" + "'" + userID + "'";

                PreparedStatement statement = database.prepareStatement(sql);
                statement.executeQuery();

                while (statement.getResultSet().next()) {
                    String alias = statement.getResultSet().getString("alias");
                    String profile_picture = statement.getResultSet().getString("profile_picture");
                    String user_id_1 = statement.getResultSet().getString("user_id");
                    response.add(new FriendResponseClass(alias, profile_picture, user_id_1));
                }
                statement.close();
                database.close();
            } catch (SQLException e) {
                return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @RequestMapping(value = "/fetch_following", method = RequestMethod.POST)
    public ResponseEntity<ArrayList<FriendResponseClass>> fetch_following(@RequestBody String input) {

        String userID;
        Connection database;
        ArrayList<FriendResponseClass> response = new ArrayList<>();

        if (input != null) {
            userID = input;
            database = CommonItems.getConnection();

            try {

                String sql = "SELECT alias, profile_picture, public.profile.user_id \n " +
                        "FROM public.profile \n" +
                        "JOIN public.friends on profile.user_id = friends.friend_user_id \n" +
                        "WHERE friends.user_id = " + "'" + userID + "'";

                PreparedStatement statement = database.prepareStatement(sql);
                statement.executeQuery();

                while (statement.getResultSet().next()) {
                    String alias = statement.getResultSet().getString("alias");
                    String profile_picture = statement.getResultSet().getString("profile_picture");
                    String user_id_1 = statement.getResultSet().getString("user_id");

                    response.add(new FriendResponseClass(alias, profile_picture, user_id_1));
                }

                statement.close();
                database.close();

            } catch (SQLException e) {
                return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @RequestMapping(value = "/fetch_profile", method = RequestMethod.POST)
    public ResponseEntity<ProfileResponseClass> fetch_profile(@RequestBody String input) {

        String userID;
        Connection database;
        ProfileResponseClass response = null;

        if (input != null) {
            userID = input;
            database = CommonItems.getConnection();

            try {

                String sql = "SELECT * FROM public.profile WHERE user_id = '" + userID + "'";

                PreparedStatement statement = database.prepareStatement(sql);
                statement.executeQuery();
                statement.getResultSet().next();
                String profile_picture = statement.getResultSet().getString("profile_picture");
                String alias = statement.getResultSet().getString("alias");
                String json = statement.getResultSet().getString("notification_preferences");
                response = new ProfileResponseClass(profile_picture, alias, json);
                statement.close();
                database.close();

            } catch (SQLException e) {
                return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @RequestMapping(value = "/unfollow", method = RequestMethod.POST)
    public ResponseEntity<ServerResponse> unfollow(@RequestBody FriendInput input) {

        String userId;
        String friendUserId;
        Connection database;

        if (input != null) {
            userId = input.getUserId();
            friendUserId = input.getFriendUserId();
            database = CommonItems.getConnection();

            try {
                String sql = "DELETE FROM public.friends * "
                        + "WHERE friend_user_id = '" + friendUserId + "' "
                        + "AND user_id = '" + userId + "'";

                Statement statement = database.createStatement();
                statement.executeUpdate(sql);
                statement.close();
                database.close();

            } catch (SQLException e) {
                return new ResponseEntity<>(new ServerResponse(String.valueOf(QUERY_BAD)), HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }
        return new ResponseEntity<>(new ServerResponse(String.valueOf(QUERY_GOOD)), HttpStatus.OK);
    }

    @RequestMapping(value = "/follow", method = RequestMethod.POST)
    public ResponseEntity<ServerResponse> follow(@RequestBody FriendInput input) {

        String userId;
        String friendUserId;
        Connection database;

        if (input != null) {
            userId = input.getUserId();
            friendUserId = input.getFriendUserId();
            database = CommonItems.getConnection();

            try {

                String sql = "INSERT INTO public.friends (friend_user_id, user_id)\n" +
                        "SELECT '" + friendUserId + "', '" + userId + "'\n" +
                        "WHERE NOT EXISTS(\n" +
                        "    SELECT friend_user_id, user_id\n" +
                        "    FROM public.friends\n" +
                        "    WHERE user_id = '" + userId + "'\n" +
                        "\tAND friend_user_id = '" + friendUserId + "'\n" +
                        ")\n";

                Statement statement = database.createStatement();
                statement.executeUpdate(sql);
                statement.close();
                database.close();

            } catch (SQLException e) {
                return new ResponseEntity<>(new ServerResponse(String.valueOf(QUERY_BAD)), HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }
        return new ResponseEntity<>(new ServerResponse(String.valueOf(QUERY_GOOD)), HttpStatus.OK);
    }

    @RequestMapping(value = "/check_user_existance", method = RequestMethod.POST)
    public ResponseEntity<ServerResponse> check_user_existance(@RequestBody String input) {

        String userID;
        Connection database;
        String result = null;

        if (input != null) {
            userID = input;
            database = CommonItems.getConnection();

            try {
                String sql = "select public.check_profile('" + userID + "');";
                PreparedStatement statement = database.prepareStatement(sql);
                statement.executeQuery();
                statement.getResultSet().next();
                result = statement.getResultSet().getString("check_profile");
                statement.close();
                database.close();

            } catch (SQLException e) {
                return new ResponseEntity<>(new ServerResponse(String.valueOf(QUERY_BAD)), HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }
        return new ResponseEntity<>(new ServerResponse(result), HttpStatus.OK);
    }

    @RequestMapping(value = "/fetch_follow_info", method = RequestMethod.POST)
    public ResponseEntity<FollowResponseClass> fetch_follow_info(@RequestBody String input) {

        String userID;
        Connection database;
        FollowResponseClass response = null;

        if (input != null) {
            userID = input;
            database = CommonItems.getConnection();

            try {

                String sql = "SELECT (SELECT COUNT(*) FROM public.friends \n" +
                        "WHERE public.friends.user_id = '" + userID + "') AS following, (SELECT COUNT(*) FROM public.friends \n" +
                        "WHERE public.friends.friend_user_id = '" + userID + "') AS followed_by FROM public.profile where user_id = '" + userID + "'\n";

                PreparedStatement statement = database.prepareStatement(sql);
                statement.executeQuery();
                statement.getResultSet().next();
                int following = statement.getResultSet().getInt("following");
                int followed_by = statement.getResultSet().getInt("followed_by");
                response = new FollowResponseClass(following, followed_by);
                statement.close();
                database.close();

            } catch (SQLException e) {
                return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @RequestMapping(value = "/update_profile", method = RequestMethod.POST)
    public ResponseEntity<ServerResponse> update_profile(@RequestBody ProfileRequestClass input) {

        String userID;
        String pictureUrl;
        String alias;
        Connection database;
        String result;

        if (input != null) {
            userID = input.getUserID();
            pictureUrl = input.getPictureUrl();
            alias = input.getAlias();
            database = CommonItems.getConnection();

            try {

                String sql = "SELECT public.update_profile('" + userID + "', '" + alias + "', '" + pictureUrl + "');";
                PreparedStatement statement = database.prepareStatement(sql);
                statement.executeQuery();
                statement.getResultSet().next();
                result = statement.getResultSet().getString("update_profile");
                statement.close();
                database.close();

                if (result.equalsIgnoreCase("PHOTO_UPLOAD_OR_NO_CHANGE")) {
                    updateCartoDBRecords(PHOTO_UPLOAD_OR_NO_CHANGE, userID, pictureUrl, null);
                    return new ResponseEntity<>(new ServerResponse(String.valueOf(PHOTO_UPLOAD_OR_NO_CHANGE)), HttpStatus.OK);
                }
                if (result.equalsIgnoreCase("USERNAME_IN_USE")) {
                    return new ResponseEntity<>(new ServerResponse(String.valueOf(USERNAME_IN_USE)), HttpStatus.OK);
                }
                if (result.equalsIgnoreCase("PHOTO_AND_OR_USERNAME_UPDATED")) {
                    updateCartoDBRecords(PHOTO_AND_OR_USERNAME_UPDATED, userID, pictureUrl, alias);
                    return new ResponseEntity<>(new ServerResponse(String.valueOf(PHOTO_AND_OR_USERNAME_UPDATED)), HttpStatus.OK);
                }
                if (result.equalsIgnoreCase("NEW_USER_ADDED")) {
                    return new ResponseEntity<>(new ServerResponse(String.valueOf(NEW_USER_ADDED)), HttpStatus.OK);
                }

            } catch (SQLException e) {
                return new ResponseEntity<>(new ServerResponse(String.valueOf(QUERY_BAD)), HttpStatus.INTERNAL_SERVER_ERROR);
            }

        }
        return new ResponseEntity<>(new ServerResponse(String.valueOf(QUERY_BAD)), HttpStatus.INTERNAL_SERVER_ERROR);
    }

    private static void updateCartoDBRecords(int result, String userID, String pictureUrl, String alias) {

        SecuredCartoDBClient cartoDBCLient = App.CartoDBBackend.Utils.CommonItems.getClient();

        String sql;

        try {

            switch (result) {
                case PHOTO_UPLOAD_OR_NO_CHANGE:

                    sql = "update questions\n" +
                            "set profile_picture = '" + pictureUrl + "'\n" +
                            "where user_id = '" + userID + "';\n" +
                            "update question_answers\n" +
                            "set profile_picture = '" + pictureUrl + "'\n" +
                            "where user_id = '" + userID + "';";

                    cartoDBCLient.executeQuery(sql);

                    break;
                case PHOTO_AND_OR_USERNAME_UPDATED:

                    sql = "update questions\n" +
                            "set profile_picture = '" + pictureUrl + "', name = '" + alias + "'\n" +
                            "where user_id = '" + userID + "';\n" +
                            "update question_answers\n" +
                            "set profile_picture = '" + pictureUrl + "', name = '" + alias + "'\n" +
                            "where user_id = '" + userID + "';";

                    cartoDBCLient.executeQuery(sql);

                    break;
            }
        } catch (CartoDBException e) {
            e.printStackTrace();
        }
    }

    @RequestMapping(value = "/update_user_settings", method = RequestMethod.POST)
    public ResponseEntity<ServerResponse> update_user_settings(@RequestBody UserSettingsRequestClass input) {

        String userID;
        String json;
        Connection database;

        if (input != null) {
            userID = input.getUserID();
            json = input.getJson();
            database = CommonItems.getConnection();

            try {
                Statement statement = database.createStatement();
                String sql = "update public.profile set notification_preferences = '" + json + "' where user_id = '" + userID + "'";
                statement.executeUpdate(sql);
                statement.close();
                database.close();
            } catch (SQLException e) {
                return new ResponseEntity<>(new ServerResponse(String.valueOf(QUERY_BAD)), HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }
        return new ResponseEntity<>(new ServerResponse(String.valueOf(QUERY_GOOD)), HttpStatus.OK);
    }

    @RequestMapping(value = "/fetch_external_profile", method = RequestMethod.POST)
    public ResponseEntity<ProfileResponseClass> fetch_external_profile(@RequestBody String input) {

        String aliasInput;
        Connection database;
        ProfileResponseClass response = null;

        if (input != null) {
            aliasInput = input;
            database = CommonItems.getConnection();

            try {

                String sql = "select * from public.profile where alias = '" + aliasInput + "'";

                PreparedStatement statement = database.prepareStatement(sql);
                statement.executeQuery();
                statement.getResultSet().next();
                String profile_picture = statement.getResultSet().getString("profile_picture");
                String alias = statement.getResultSet().getString("alias");
                String userID = statement.getResultSet().getString("user_id");
                response = new ProfileResponseClass(profile_picture, alias, userID);
                statement.close();
                database.close();

            } catch (SQLException e) {
                return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @RequestMapping(value = "/check_if_friend", method = RequestMethod.POST)
    public ResponseEntity<ServerResponse> check_if_friend(@RequestBody FriendInput input) {

        String userId;
        String friendUserId;
        Connection database;

        int answer = 0;

        if (input != null) {
            userId = input.getUserId();
            friendUserId = input.getFriendUserId();
            database = CommonItems.getConnection();

            try {

                String sql = "select 200 as answer from public.friends where public.friends.user_id = '" + userId + "' and public.friends.friend_user_id = '" + friendUserId + "'";

                PreparedStatement statement = database.prepareStatement(sql);
                statement.executeQuery();

                while (statement.getResultSet().next()) {
                    statement.getResultSet().next();
                    answer = statement.getResultSet().getInt("answer");
                }

                statement.close();
                database.close();


            } catch (SQLException e) {
                return new ResponseEntity<>(new ServerResponse(String.valueOf(QUERY_BAD)), HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }
        return new ResponseEntity<>(new ServerResponse(String.valueOf(answer)), HttpStatus.OK);
    }

    @RequestMapping(value = "/find_friends", method = RequestMethod.POST)
    public ResponseEntity<ArrayList<FriendResponseClass>> find_friends(@RequestBody FriendInput input) {

        Connection database;
        String friendName;
        String userID;
        ArrayList<FriendResponseClass> response = new ArrayList<>();

        if (input != null) {
            database = CommonItems.getConnection();
            friendName = input.getFriendUserId();
            userID = input.getUserId();

            try {

                String sql = "select * from public.profile where alias like '%" + friendName + "%' AND user_id not like '" + userID + "'";

                PreparedStatement statement = database.prepareStatement(sql);
                statement.executeQuery();

                while (statement.getResultSet().next()) {
                    String alias = statement.getResultSet().getString("alias");
                    String profile_picture = statement.getResultSet().getString("profile_picture");
                    String user_id = statement.getResultSet().getString("user_id");

                    response.add(new FriendResponseClass(alias, profile_picture, user_id));
                }

                statement.close();
                database.close();

            } catch (SQLException e) {
                return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

}
