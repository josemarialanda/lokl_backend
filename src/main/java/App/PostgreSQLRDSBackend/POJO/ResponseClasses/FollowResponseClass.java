package App.PostgreSQLRDSBackend.POJO.ResponseClasses;

public class FollowResponseClass {
	
	int following;
	int followed_by;
	
	public FollowResponseClass() {
		
	}

	public FollowResponseClass(int following, int followed_by) {
		super();
		this.following = following;
		this.followed_by = followed_by;
	}

	public int getFollowing() {
		return following;
	}

	public void setFollowing(int following) {
		this.following = following;
	}

	public int getFollowed_by() {
		return followed_by;
	}

	public void setFollowed_by(int followed_by) {
		this.followed_by = followed_by;
	}
	
	

}
