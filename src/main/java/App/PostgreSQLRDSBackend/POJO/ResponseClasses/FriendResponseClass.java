package App.PostgreSQLRDSBackend.POJO.ResponseClasses;

public class FriendResponseClass {
	
	String name;
	String profilePicture;
	String user_id;
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getProfilePicture() {
		return profilePicture;
	}

	public void setProfilePicture(String profilePicture) {
		this.profilePicture = profilePicture;
	}

	public String getUser_id() {
		return user_id;
	}

	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}

	public FriendResponseClass() {

	}

	public FriendResponseClass(String name, String profilePicture, String user_id) {
		super();
		this.name = name;
		this.profilePicture = profilePicture;
		this.user_id = user_id;
	}

	
	
	

}
