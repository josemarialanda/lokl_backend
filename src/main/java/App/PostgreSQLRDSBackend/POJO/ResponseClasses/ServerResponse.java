package App.PostgreSQLRDSBackend.POJO.ResponseClasses;

public class ServerResponse {

    String response;

    public ServerResponse(String response) {
        this.response = response;
    }

    public ServerResponse() {

    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }
}
