package App.PostgreSQLRDSBackend.Utils;

import App.PostgreSQLRDSBackend.Constants;

import java.sql.Connection;
import java.sql.DriverManager;

public class CommonItems {

    public static Connection getConnection() {
        Connection connection = null;

        try {
            Class.forName("org.postgresql.Driver");
            connection = DriverManager
                    .getConnection(Constants.CONNECTION_URL, Constants.USERNAME, Constants.PASSWORD);
            connection.setAutoCommit(true);
        } catch (Exception e) {
            e.printStackTrace();
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            System.exit(0);
        }
        return connection;
    }
}
