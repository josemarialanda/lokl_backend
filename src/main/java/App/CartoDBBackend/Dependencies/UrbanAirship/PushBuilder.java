package App.CartoDBBackend.Dependencies.UrbanAirship;

import org.apache.http.HttpResponse;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.auth.BasicScheme;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.HTTP;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URI;

public class PushBuilder {

    String name;
    String namedUser;
    String questionID;
    String TYPE;
    String json;

    public PushBuilder(String name, String namedUser, String questionID, String TYPE) {
        this.name = name;
        this.namedUser = namedUser;
        this.questionID = questionID;
        this.TYPE = TYPE;
        if (TYPE.equals("QUESTION")) {
            json = "{\n" +
                    "          \"audience\": {\n" +
                    "                \"named_user\":\"" + namedUser + "\"\n" +
                    "          },\n" +
                    "          \"notification\": {\n" +
                    "             \"alert\": \"" + name + " has sent you a question!\",\n" +
                    "      \"ios\": {\n" +
                    "         \"extra\": { \"questionID\": \"" + questionID + "\"}\n" +
                    "      },\n" +
                    "     \"android\": {\n" +
                    "         \"extra\": { \"questionID\": \"" + questionID + "\"}\n" +
                    "      }\n" +
                    "          },\n" +
                    "          \"device_types\": [\"ios\", \"android\"]\n" +
                    "       }";
        }
        if (TYPE.equals("ANSWER")) {
            json = "{\n" +
                    "          \"audience\": {\n" +
                    "                \"named_user\":\"" + namedUser + "\"\n" +
                    "          },\n" +
                    "          \"notification\": {\n" +
                    "             \"alert\": \"" + name + " has answered one of your questions!\",\n" +
                    "      \"ios\": {\n" +
                    "         \"extra\": { \"questionID\": \"" + questionID + "\"}\n" +
                    "      },\n" +
                    "     \"android\": {\n" +
                    "         \"extra\": { \"questionID\": \"" + questionID + "\"}\n" +
                    "      }\n" +
                    "          },\n" +
                    "          \"device_types\": [\"ios\", \"android\"]\n" +
                    "       }";
        }

    }

    @SuppressWarnings("deprecation")
    public void sendPush() {

        try {
            DefaultHttpClient httpClient = new DefaultHttpClient();
            HttpPost postRequest = new HttpPost();
            URI uri = new URI("https://go.urbanairship.com/api/push");
            postRequest.setURI(uri);
            postRequest.setHeader(BasicScheme.authenticate(
                    new UsernamePasswordCredentials("y0g6tX42QeW0w6DNwnngoA", "W0GT-XTZTm6E_8tcaoR1AQ"),
                    HTTP.UTF_8, false));
            postRequest.addHeader("Accept", "application/vnd.urbanairship+json; version=3");
            postRequest.addHeader("Content-type", "application/json");

            StringEntity input = new StringEntity(json);
            input.setContentType(ContentType.APPLICATION_JSON.toString());
            postRequest.setEntity(input);

            HttpResponse response = httpClient.execute(postRequest);

            if (response.getEntity().getContentLength() != 0) {
                StringBuilder sb = new StringBuilder();
                try {
                    BufferedReader reader =
                            new BufferedReader(new InputStreamReader(response.getEntity().getContent()), 65728);
                    String line;

                    while ((line = reader.readLine()) != null) {
                        sb.append(line);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}


