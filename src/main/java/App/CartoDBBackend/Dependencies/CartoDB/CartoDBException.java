package App.CartoDBBackend.Dependencies.CartoDB;

public class CartoDBException extends Exception {

    public CartoDBException(String msg) {
        super(msg);
    }

}