package App.CartoDBBackend.POJO.RequestClasses;

public class AnswerFetchRequestClass {
	
	double latitude;
	double longitude;
	String questionID;
	
	public AnswerFetchRequestClass(){
		
	}
	
	public AnswerFetchRequestClass(double latitude, double longitude, String questionID) {
		this.latitude = latitude;
		this.longitude = longitude;
		this.questionID = questionID;
	}
	
	public double getLatitude() {
		return latitude;
	}
	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}
	public double getLongitude() {
		return longitude;
	}
	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}
	public String getQuestionID() {
		return questionID;
	}
	public void setQuestionID(String questionID) {
		this.questionID = questionID;
	}

}
