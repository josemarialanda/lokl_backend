package App.CartoDBBackend.POJO.RequestClasses;

import java.io.Serializable;
import java.util.List;

public class PushRequestClass implements Serializable{

	double latitude;
    double longitude;
    String name;
    String text;
    String questionID;
    String userID;
    String imageURL;
    String namedUser;
    int radius;
    String profilePicture;
	List<String> targetUsers;
    List<String> hastags;

	public PushRequestClass(){
    	
    }
    
	public PushRequestClass(double latitude, double longitude, String name, String text, String questionID, String userID, String imageURL, String namedUser, int radius, String profilePicture, List<String> targetUsers, List<String> hastags) {
		this.latitude = latitude;
		this.longitude = longitude;
		this.name = name;
		this.text = text;
		this.questionID = questionID;
		this.userID = userID;
		this.imageURL = imageURL;
		this.namedUser = namedUser;
		this.radius = radius;
		this.profilePicture = profilePicture;
		this.targetUsers = targetUsers;
        this.hastags = hastags;
	}
	
	public double getLatitude() {
		return latitude;
	}
	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}
	public double getLongitude() {
		return longitude;
	}
	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public String getQuestionID() {
		return questionID;
	}
	public void setQuestionID(String questionID) {
		this.questionID = questionID;
	}
	public String getUserID() {
		return userID;
	}
	public void setUserID(String userID) {
		this.userID = userID;
	}
	public String getImageURL() {
		return imageURL;
	}
	public void setImageURL(String imageURL) {
		this.imageURL = imageURL;
	}
	public String getNamedUser() {
		return namedUser;
	}

	public void setNamedUser(String namedUser) {
		this.namedUser = namedUser;
	}
	public int getRadius() {
		return radius;
	}

	public void setRadius(int radius) {
		this.radius = radius;
	}
	
	public String getProfilePicture() {
		return profilePicture;
	}

	public void setProfilePicture(String profilePicture) {
		this.profilePicture = profilePicture;
	}

	public List<String> getTargetUsers() {
		return targetUsers;
	}

	public void setTargetUsers(List<String> targetUsers) {
		this.targetUsers = targetUsers;
	}

    public List<String> getHastags() {
        return hastags;
    }

    public void setHastags(List<String> hastags) {
        this.hastags = hastags;
    }
}
