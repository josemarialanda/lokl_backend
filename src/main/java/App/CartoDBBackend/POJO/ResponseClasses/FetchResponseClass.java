package App.CartoDBBackend.POJO.ResponseClasses;

public class FetchResponseClass {

	String name;
    String text;
    String questionID;
    String userID;
    String date_time;
    String distance;
    String imageURL;
    String location;
    String answer_count;
    String profilePicture;
    String latitude;
    String longitude;

	public FetchResponseClass() {
	}

	public FetchResponseClass(String name, String text, String questionID, String userID, String date_time,
			String distance, String imageURL, String location, String answer_count, String profilePicture, String latitude, String longitude) {
		super();
		this.name = name;
		this.text = text;
		this.questionID = questionID;
		this.userID = userID;
		this.date_time = date_time;
		this.distance = distance;
		this.imageURL = imageURL;
		this.location = location;
		this.answer_count = answer_count;
		this.profilePicture = profilePicture;
		this.latitude = latitude;
		this.longitude = longitude;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getQuestionID() {
		return questionID;
	}

	public void setQuestionID(String questionID) {
		this.questionID = questionID;
	}

	public String getUserID() {
		return userID;
	}

	public void setUserID(String userID) {
		this.userID = userID;
	}

	public String getDate_time() {
		return date_time;
	}

	public void setDate_time(String date_time) {
		this.date_time = date_time;
	}

	public String getDistance() {
		return distance;
	}

	public void setDistance(String distance) {
		this.distance = distance;
	}

	public String getImageURL() {
		return imageURL;
	}

	public void setImageURL(String imageURL) {
		this.imageURL = imageURL;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getAnswer_count() {
		return answer_count;
	}

	public void setAnswer_count(String answer_count) {
		this.answer_count = answer_count;
	}
	
	public String getProfilePicture() {
		return profilePicture;
	}

	public void setProfilePicture(String profilePicture) {
		this.profilePicture = profilePicture;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}
	
}
