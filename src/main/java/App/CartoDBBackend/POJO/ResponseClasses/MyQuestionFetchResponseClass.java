package App.CartoDBBackend.POJO.ResponseClasses;

public class MyQuestionFetchResponseClass {
	
	String questionID;
    String questionText;
    String imageURL;
    String name;
    String address;
    String date_time;
    String profilePicture;
    String answerCount;
    String userID;
    String latitude;
    String longitude;
    
    public MyQuestionFetchResponseClass(){
    	
    }

	public MyQuestionFetchResponseClass(String questionID, String questionText, String imageURL, String name,
			String address, String date_time, String profilePicture, String answerCount, String userID, String latitude, String longitude) {
		super();
		this.questionID = questionID;
		this.questionText = questionText;
		this.imageURL = imageURL;
		this.name = name;
		this.address = address;
		this.date_time = date_time;
		this.profilePicture = profilePicture;
		this.answerCount = answerCount;
		this.userID = userID;
		this.latitude = latitude;
		this.longitude = longitude;
	}

	public String getQuestionID() {
		return questionID;
	}

	public void setQuestionID(String questionID) {
		this.questionID = questionID;
	}

	public String getQuestionText() {
		return questionText;
	}

	public void setQuestionText(String questionText) {
		this.questionText = questionText;
	}

	public String getImageURL() {
		return imageURL;
	}

	public void setImageURL(String imageURL) {
		this.imageURL = imageURL;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getDate_time() {
		return date_time;
	}

	public void setDate_time(String date_time) {
		this.date_time = date_time;
	}

	public String getProfilePicture() {
		return profilePicture;
	}

	public void setProfilePicture(String profilePicture) {
		this.profilePicture = profilePicture;
	}

	public String getAnswerCount() {
		return answerCount;
	}

	public void setAnswerCount(String answerCount) {
		this.answerCount = answerCount;
	}

	public String getUserID() {
		return userID;
	}

	public void setUserID(String userID) {
		this.userID = userID;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}
	
}
