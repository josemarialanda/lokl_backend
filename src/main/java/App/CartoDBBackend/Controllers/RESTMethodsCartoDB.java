package App.CartoDBBackend.Controllers;

import java.io.IOException;
import java.net.MalformedURLException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import App.CartoDBBackend.Dependencies.CartoDB.CartoDBException;
import App.CartoDBBackend.Dependencies.CartoDB.impl.SecuredCartoDBClient;
import App.CartoDBBackend.Dependencies.CartoDB.model.CartoDBResponse;
import App.CartoDBBackend.Utils.CommonItems;
import App.CartoDBBackend.POJO.RequestClasses.AnswerFetchRequestClass;
import App.CartoDBBackend.POJO.RequestClasses.DeleteRequestClass;
import App.CartoDBBackend.POJO.RequestClasses.FetchRequestClass;
import App.CartoDBBackend.POJO.RequestClasses.PushRequestClass;
import App.CartoDBBackend.POJO.ResponseClasses.FetchResponseClass;
import App.CartoDBBackend.POJO.ResponseClasses.MyQuestionFetchResponseClass;
import App.CartoDBBackend.Dependencies.UrbanAirship.PushBuilder;
import App.CartoDBBackend.Utils.ReverseGeocoding;
import App.PostgreSQLRDSBackend.POJO.ResponseClasses.ServerResponse;
import org.json.simple.parser.ParseException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class RESTMethodsCartoDB {

    private static final int QUERY_GOOD = 2;
    private static final int QUERY_BAD = -2;

    private static final int ANSWERS_PRESENT = 1;
    private static final int ANSWERS_NOT_PRESENT = -2;

    private static final int QUESTIONS_PRESENT = 1;
    private static final int QUESTIONS_NOT_PRESENT = -2;

    @RequestMapping(value = "/fetch_questions", method = RequestMethod.POST)
    public ResponseEntity<ArrayList<FetchResponseClass>> fetch_questions(@RequestBody FetchRequestClass input) {

        ArrayList<FetchResponseClass> response = new ArrayList<>();
        double latitude;
        double longitude;
        String userID;
        Connection database;
        List<String> friends_user_id = new ArrayList<>();

        if (input != null) {

            latitude = input.getLatitude();
            longitude = input.getLongitude();
            userID = input.getUserID();
            database = App.PostgreSQLRDSBackend.Utils.CommonItems.getConnection();

            try {

                String sql = "select friend_user_id from public.friends where user_id = " + "'" + userID + "'";
                PreparedStatement statement = database.prepareStatement(sql);
                statement.executeQuery();

                while (statement.getResultSet().next()) {
                    String user_id = statement.getResultSet().getString("friend_user_id");
                    friends_user_id.add(user_id);
                }
                statement.close();
                database.close();
            } catch (SQLException e) {
                return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
            }

            String sql;

            if (!friends_user_id.isEmpty()) {

                StringBuilder builder = new StringBuilder();
                for (int i = 0; i < friends_user_id.size(); i++) {

                    if (i == friends_user_id.size() - 1) {
                        builder.append("'" + friends_user_id.get(i) + "'");
                    } else {
                        builder.append("'" + friends_user_id.get(i) + "'" + ",");
                    }
                }

                String formattedText = builder.toString();

                sql = "SELECT \n" +
                        "ST_X(ST_Centroid(the_geom))::text as latitude,\n" +
                        "ST_Y(ST_Centroid(the_geom))::text as longitude,\n" +
                        "round(st_distance(ST_Centroid(the_geom)::geography,'POINT(" + longitude + " "
                        + latitude + ")'::geography))::text as distance, main.name, main.question_text, main.picture, main.question_id, main.user_id, " +
                        "(TO_CHAR(((EXTRACT(EPOCH FROM (current_timestamp - date_time))) || ' second')::interval, 'HH24:MI:SS')) as date_time, main.address, main.profile_picture, \n" +
                        "       (SELECT COUNT(qa.question_id)\n" +
                        "        FROM  questions q\n" +
                        "        LEFT JOIN question_answers qa \n" +
                        "        ON qa.question_id = q.question_id\n" +
                        "        WHERE q.question_id = main.question_id\n" +
                        "        GROUP BY q.question_id)::text as answer_count \n" +
                        "FROM questions AS main\n" +
                        "WHERE ST_Intersects(ST_Buffer(ST_GeomFromText('POINT(" + longitude + " " + latitude + ")',\n" +
                        "       4326)::geography, 1000)::geometry, the_geom) AND (NOW() - INTERVAL '24 HOUR') < date_time AND user_id NOT LIKE '" + userID + "' \n" +
                        "OR\n" +
                        "(\n" +
                        "  user_id IN (" + formattedText + ") \n" +
                        "  AND (NOW() - INTERVAL '24 HOUR') < date_time\n" +
                        ")\n" +
                        "\n" +
                        "ORDER BY date_time ASC\n";
            } else {
                sql = "SELECT \n" +
                        "ST_X(ST_Centroid(the_geom))::text as latitude,\n" +
                        "ST_Y(ST_Centroid(the_geom))::text as longitude,\n" +
                        "round(st_distance(ST_Centroid(the_geom)::geography,'POINT(" + longitude + " "
                        + latitude + ")'::geography))::text as distance, main.name, main.question_text, main.picture, main.question_id, main.user_id, " +
                        "(TO_CHAR(((EXTRACT(EPOCH FROM (current_timestamp - date_time))) || ' second')::interval, 'HH24:MI:SS')) as date_time, main.address, main.profile_picture, \n" +
                        "       (SELECT COUNT(qa.question_id)\n" +
                        "        FROM  questions q\n" +
                        "        LEFT JOIN question_answers qa \n" +
                        "        ON qa.question_id = q.question_id\n" +
                        "        WHERE q.question_id = main.question_id\n" +
                        "        GROUP BY q.question_id)::text as answer_count \n" +
                        "FROM questions AS main\n" +
                        "WHERE ST_Intersects(ST_Buffer(ST_GeomFromText('POINT(" + longitude + " " + latitude + ")',\n" +
                        "       4326)::geography, 1000)::geometry, the_geom) AND (NOW() - INTERVAL '24 HOUR') < date_time AND user_id NOT LIKE '" + userID + "'\n" +
                        "ORDER BY date_time ASC";
            }


            SecuredCartoDBClient cartoDBCLient = CommonItems.getClient();

            try {

                CartoDBResponse<Map<String, String>> res = cartoDBCLient.request(sql);

                for (Map<String, String> currentQuestion : res.getRows()) {
                    response.add(new FetchResponseClass(
                            currentQuestion.get("name"),
                            currentQuestion.get("question_text"),
                            currentQuestion.get("question_id"),
                            currentQuestion.get("user_id"),
                            currentQuestion.get("date_time"),
                            currentQuestion.get("distance"),
                            currentQuestion.get("picture"),
                            currentQuestion.get("address"),
                            currentQuestion.get("answer_count"),
                            currentQuestion.get("profile_picture"),
                            currentQuestion.get("latitude"),
                            currentQuestion.get("longitude")));
                }

            } catch (CartoDBException e) {
                return null;
            }

        }
        return new ResponseEntity<ArrayList<FetchResponseClass>>(response, HttpStatus.OK);
    }

    @RequestMapping(value = "/fetch_answers", method = RequestMethod.POST)
    public ResponseEntity<ArrayList<FetchResponseClass>> fetch_answers(@RequestBody AnswerFetchRequestClass input) {

        ArrayList<FetchResponseClass> response = new ArrayList<>();
        double latitude;
        double longitude;
        String questionID;

        if (input != null) {
            latitude = input.getLatitude();
            longitude = input.getLongitude();
            questionID = input.getQuestionID();

            SecuredCartoDBClient cartoDBCLient = CommonItems.getClient();

            try {

                String sql = "SELECT (TO_CHAR(((EXTRACT(EPOCH FROM (current_timestamp - date_time))) || ' second')::interval, 'HH24:MI:SS')) as date_time, " +
                        "name, user_id, picture, answer_text, profile_picture,round(st_distance(\n" +
                        "ST_Centroid(the_geom)::geography,'POINT(" + longitude + " " + latitude + ")'::geography))::text as distance\n" +
                        "FROM question_answers\n" +
                        "WHERE question_id = '" + questionID + "'\n" +
                        "AND (NOW() - INTERVAL '24 HOUR') < date_time\n" +
                        "ORDER BY date_time DESC";

                CartoDBResponse<Map<String, String>> res = cartoDBCLient.request(sql);

                for (Map<String, String> currentAnswer : res.getRows()) {
                    response.add(new FetchResponseClass(currentAnswer.get("name"), currentAnswer.get("answer_text"), "", currentAnswer.get("user_id"), currentAnswer.get("date_time"), currentAnswer.get("distance"), currentAnswer.get("picture"), "", "", currentAnswer.get("profile_picture"), "", ""));
                }

            } catch (CartoDBException e) {
                return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
            }

        }
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @RequestMapping(value = "/push_question", method = RequestMethod.POST)
    public ResponseEntity<ServerResponse> push_question(@RequestBody PushRequestClass input) {

        double latitude;
        double longitude;
        String name;
        String text;
        String questionID;
        String userID;
        String imageURL;
        int radius;
        String location = null;
        String profilePicture;
        List<String> targetUsers;
        List<String> hashTags;

        if (input != null) {
            latitude = input.getLatitude();
            longitude = input.getLongitude();
            name = input.getName();
            text = input.getText();
            questionID = input.getQuestionID();
            userID = input.getUserID();
            imageURL = input.getImageURL();
            radius = input.getRadius();
            profilePicture = input.getProfilePicture();
            targetUsers = input.getTargetUsers();
            hashTags = input.getHastags();

            SecuredCartoDBClient cartoDBCLient = CommonItems.getClient();

            try {
                location = ReverseGeocoding.getAddressByGpsCoordinates(String.valueOf(longitude), String.valueOf(latitude));
            } catch (MalformedURLException e1) {
                // TODO Auto-generated catch block
                e1.printStackTrace();
            } catch (IOException e1) {
                // TODO Auto-generated catch block
                e1.printStackTrace();
            } catch (ParseException e1) {
                // TODO Auto-generated catch block
                e1.printStackTrace();
            }

            String mainSQL;

            if (!hashTags.isEmpty()) {
                StringBuilder builder = new StringBuilder();
                for (int i = 0; i < hashTags.size(); i++) {
                    if (i == hashTags.size() - 1) {
                        builder.append(hashTags.get(i));
                    } else {
                        builder.append(hashTags.get(i)).append(",");
                    }
                }
                String formattedText = "'" + builder.toString() + "'";

                mainSQL = "INSERT INTO questions (the_geom, name, question_id, question_text, user_id, date_time, picture, address, profile_picture, hashtags) "
                        + "SELECT ST_Buffer(ST_GeomFromText('POINT(" + longitude + " " + latitude + ")', 4326)::geography, " + radius + ")::geometry, '"
                        + name + "', '" + questionID + "', '" + text + "', '" + userID + "', " +
                        "current_timestamp" + " , '" + imageURL + "', '" + location + "', " + "'" + profilePicture + "', " + "(" + formattedText + ")";
            } else {
                mainSQL = "INSERT INTO questions (the_geom, name, question_id, question_text, user_id, date_time, picture, address, profile_picture) "
                        + "SELECT ST_Buffer(ST_GeomFromText('POINT(" + longitude + " " + latitude + ")', 4326)::geography, " + radius + ")::geometry, '"
                        + name + "', '" + questionID + "', '" + text + "', '" + userID + "', " +
                        "current_timestamp" + " , '" + imageURL + "', '" + location + "', " + "'" + profilePicture + "'";
            }

            try {

                cartoDBCLient.executeQuery(mainSQL);

            } catch (CartoDBException e) {
                return new ResponseEntity<>(new ServerResponse(e.getLocalizedMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
            }

            if (!targetUsers.isEmpty()) {

                Connection database = App.PostgreSQLRDSBackend.Utils.CommonItems.getConnection();

                StringBuilder builder = new StringBuilder();
                for (int i = 0; i < targetUsers.size(); i++) {

                    if (i == targetUsers.size() - 1) {
                        builder.append("'").append(targetUsers.get(i)).append("'");
                    } else {
                        builder.append("'").append(targetUsers.get(i)).append("'").append(",");
                    }
                }
                String aliasTextList = builder.toString();

                ArrayList<String> userIDs = new ArrayList<>();

                try {

                    String profileSQL = "SELECT user_id \n" +
                            "FROM public.profile \n" +
                            "WHERE alias IN (" + aliasTextList + ")";

                    PreparedStatement statement = database.prepareStatement(profileSQL);
                    statement.executeQuery();

                    while (statement.getResultSet().next()) {
                        String user_id = statement.getResultSet().getString("user_id");
                        userIDs.add(user_id);
                    }
                    statement.close();
                    database.close();

                } catch (SQLException e) {
                    return new ResponseEntity<>(new ServerResponse(e.getLocalizedMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
                }

                if (!userID.isEmpty()) {

                    StringBuilder stringBuilder = new StringBuilder();
                    for (int i = 0; i < userIDs.size(); i++) {
                        if (i == userIDs.size() - 1) {
                            stringBuilder.append("('" + userIDs.get(i) + "'," + "'" + questionID + "')");
                        } else {
                            stringBuilder.append("('" + userIDs.get(i) + "'," + "'" + questionID + "'),");
                        }
                    }
                    String formattedTxt = stringBuilder.toString();

                    try {
                        String associationSQL = "INSERT INTO user_question_association (user_id, association_id) \n" +
                                "VALUES " + formattedTxt;

                        cartoDBCLient.executeQuery(associationSQL);

                    } catch (CartoDBException e) {
                        return new ResponseEntity<>(new ServerResponse(e.getLocalizedMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
                    }
                    for (String namedUser : userIDs) {
                        if (!namedUser.equals(userID)) {
                            new PushBuilder(name, namedUser, questionID, "QUESTION").sendPush();
                        }
                    }

                }
            }
        }
        return new ResponseEntity<>(new ServerResponse(String.valueOf(QUERY_GOOD)), HttpStatus.OK);
    }

    @RequestMapping(value = "/delete_question", method = RequestMethod.POST)
    public ResponseEntity<ServerResponse> delete_question(@RequestBody DeleteRequestClass input) {

        String questionID;
        String userID;

        if (input != null) {
            questionID = input.getQuestionID();
            userID = input.getUserID();

            SecuredCartoDBClient cartoDBCLient = CommonItems.getClient();

            try {

                String sql = "DELETE FROM questions WHERE user_id = '" + userID + "' AND question_id = '" + questionID + "';\n" +
                        "DELETE FROM question_answers WHERE question_id = '" + questionID + "';\n" +
                        "DELETE FROM user_question_association where association_id = '" + questionID + "'";

                cartoDBCLient.executeQuery(sql);

            } catch (CartoDBException e) {
                return new ResponseEntity<>(new ServerResponse(String.valueOf(QUERY_BAD)), HttpStatus.OK);
            }
        }
        return new ResponseEntity<>(new ServerResponse(String.valueOf(QUERY_GOOD)), HttpStatus.OK);
    }

    @RequestMapping(value = "/fetch_my_questions", method = RequestMethod.POST)
    public ResponseEntity<ArrayList<MyQuestionFetchResponseClass>> fetch_my_questions(@RequestBody String input) {

        String userID;
        ArrayList<MyQuestionFetchResponseClass> response = new ArrayList<>();

        if (input != null) {
            userID = input;

            SecuredCartoDBClient cartoDBCLient = CommonItems.getClient();

            try {

                String sql = "SELECT \n" +
                        "main.name, main.question_text, main.picture, main.question_id, main.user_id, " +
                        "(TO_CHAR(((EXTRACT(EPOCH FROM (current_timestamp - date_time))) || ' second')::interval, 'HH24:MI:SS')) as date_time, " +
                        "main.address, main.profile_picture,\n" +
                        "ST_X(ST_Centroid(the_geom))::text as latitude,\n" +
                        "ST_Y(ST_Centroid(the_geom))::text as longitude,\n" +
                        "       (SELECT COUNT(qa.question_id)\n" +
                        "        FROM  questions q\n" +
                        "        LEFT JOIN question_answers qa \n" +
                        "        ON qa.question_id = q.question_id\n" +
                        "        WHERE q.question_id = main.question_id\n" +
                        "        GROUP BY q.question_id)::text as answer_count \n" +
                        "FROM questions AS main \n" +
                        "WHERE user_id = '" + userID + "'\n" +
                        "OR question_id in (SELECT association_id FROM user_question_association where user_id = '" + userID + "')\n" +
                        "AND the_geom IS NOT NULL\n" +
                        "ORDER BY date_time ASC";

                CartoDBResponse<Map<String, String>> res = cartoDBCLient.request(sql);

                for (Map<String, String> currentQuestion : res.getRows()) {
                    response.add(new MyQuestionFetchResponseClass(
                            currentQuestion.get("question_id"),
                            currentQuestion.get("question_text"),
                            currentQuestion.get("picture"),
                            currentQuestion.get("name"),
                            currentQuestion.get("address"),
                            currentQuestion.get("date_time"),
                            currentQuestion.get("profile_picture"),
                            currentQuestion.get("answer_count"),
                            currentQuestion.get("user_id"),
                            currentQuestion.get("latitude"),
                            currentQuestion.get("longitude")));
                }

            } catch (CartoDBException e) {
                return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @RequestMapping(value = "/dissociate_question", method = RequestMethod.POST)
    public ResponseEntity<ServerResponse> dissociate_question(@RequestBody DeleteRequestClass input) {

        String questionID;
        String userID;

        if (input != null) {
            questionID = input.getQuestionID();
            userID = input.getUserID();

            SecuredCartoDBClient cartoDBCLient = CommonItems.getClient();

            try {

                String sql = "delete from user_question_association *\n" +
                        "where user_id = '" + userID + "' \n" +
                        "and association_id = '" + questionID + "'";

                cartoDBCLient.executeQuery(sql);

            } catch (CartoDBException e) {
                return new ResponseEntity<>(new ServerResponse(String.valueOf(QUERY_BAD)), HttpStatus.OK);

            }
        }
        return new ResponseEntity<>(new ServerResponse(String.valueOf(QUERY_GOOD)), HttpStatus.OK);
    }

    @RequestMapping(value = "/push_answer", method = RequestMethod.POST)
    public ResponseEntity<ServerResponse> push_answer(@RequestBody PushRequestClass input) {

        double latitude;
        double longitude;
        String name = null;
        String text = null;
        String questionID = null;
        String userID = null;
        String imageURL = null;
        String namedUser = null;
        String location = null;
        String profilePicture = null;
        SecuredCartoDBClient cartoDBCLient = null;

        if (input != null) {

            latitude = input.getLatitude();
            longitude = input.getLongitude();
            name = input.getName();
            text = input.getText();
            questionID = input.getQuestionID();
            userID = input.getUserID();
            imageURL = input.getImageURL();
            namedUser = input.getNamedUser();
            profilePicture = input.getProfilePicture();

            cartoDBCLient = CommonItems.getClient();

            try {

                String sql = "insert into question_answers (the_geom, name, question_id, answer_text, user_id, date_time, picture, profile_picture) " +
                        "values (CDB_LatLng(" + latitude + ", " + longitude + "),'" + name + "', '" + questionID + "', '" + text + "', '" + userID + "', " +
                        "current_timestamp" + ", '" + imageURL + "', " + "'" + profilePicture + "')";

                cartoDBCLient.executeQuery(sql);

            } catch (CartoDBException e) {
                return new ResponseEntity<>(new ServerResponse(String.valueOf(QUERY_BAD)), HttpStatus.OK);
            }
        }

        assert namedUser != null;
        if (!namedUser.equals(userID)) {
            new PushBuilder(name, namedUser, questionID, "ANSWER").sendPush();
        }

        try {
            String sql = "INSERT INTO user_question_association (user_id, association_id)\n" +
                    "SELECT '" + userID + "','" + questionID + "'\n" +
                    "WHERE NOT EXISTS(\n" +
                    "    SELECT user_id, association_id\n" +
                    "    FROM user_question_association\n" +
                    "    WHERE user_id = '" + userID + "'\n" +
                    "\tAND association_id = '" + questionID + "'\n" +
                    ")\n";

            cartoDBCLient.executeQuery(sql);

        } catch (CartoDBException e) {
            return new ResponseEntity<>(new ServerResponse(String.valueOf(QUERY_BAD)), HttpStatus.OK);
        }
        return new ResponseEntity<>(new ServerResponse(String.valueOf(QUERY_GOOD)), HttpStatus.OK);
    }

    @RequestMapping("/get_all_questions")
    public ResponseEntity<ArrayList<FetchResponseClass>> get_all_questions() {

        ArrayList<FetchResponseClass> response = new ArrayList<>();
        SecuredCartoDBClient cartoDBCLient = CommonItems.getClient();

        try {

            String sql = "SELECT \n" +
                    "ST_X(ST_Centroid(the_geom))::text as latitude, \n" +
                    "ST_Y(ST_Centroid(the_geom))::text as longitude, \n" +
                    "main.name, main.question_text, main.picture, main.question_id, main.user_id, main.date_time, main.address, main.profile_picture, \n" +
                    "       (SELECT COUNT(qa.question_id) \n" +
                    "        FROM  questions q \n" +
                    "        LEFT JOIN question_answers qa \n" +
                    "        ON qa.question_id = q.question_id \n" +
                    "        WHERE q.question_id = main.question_id \n" +
                    "        GROUP BY q.question_id)::text as answer_count \n" +
                    "FROM questions AS main \n" +
                    "WHERE (NOW() - INTERVAL '24 HOUR') < date_time";

            CartoDBResponse<Map<String, String>> res = cartoDBCLient.request(sql);

            for (Map<String, String> currentQuestion : res.getRows()) {
                response.add(new FetchResponseClass(
                        currentQuestion.get("name"),
                        currentQuestion.get("question_text"),
                        currentQuestion.get("question_id"),
                        currentQuestion.get("user_id"),
                        currentQuestion.get("date_time"),
                        null,
                        currentQuestion.get("picture"),
                        currentQuestion.get("address"),
                        currentQuestion.get("answer_count"),
                        currentQuestion.get("profile_picture"),
                        currentQuestion.get("latitude"),
                        currentQuestion.get("longitude")));
            }

        } catch (CartoDBException e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @RequestMapping(value = "/get_hashtag_questions", method = RequestMethod.POST)
    public ResponseEntity<ArrayList<FetchResponseClass>> get_hashtag_questions(@RequestBody String input) {

        ArrayList<FetchResponseClass> response = new ArrayList<>();

        String sql = "SELECT \n" +
                "ST_X(ST_Centroid(the_geom))::text as latitude,\n" +
                "ST_Y(ST_Centroid(the_geom))::text as longitude,\n" +
                "main.name, main.question_text, " +
                "main.picture, main.question_id, main.user_id, main.date_time, " +
                "main.address, main.profile_picture, \n" +
                "       (SELECT COUNT(qa.question_id)\n" +
                "        FROM  questions q\n" +
                "        LEFT JOIN question_answers qa \n" +
                "        ON qa.question_id = q.question_id\n" +
                "        WHERE q.question_id = main.question_id\n" +
                "        GROUP BY q.question_id)::text as answer_count \n" +
                "FROM questions AS main\n" +
                "WHERE array_to_string(ARRAY[hashtags], ',') like LOWER('%" + input + "%') \n" +
                "AND (NOW() - INTERVAL '24 HOUR') < date_time \n" +
                "ORDER BY date_time ASC\n";

        SecuredCartoDBClient cartoDBCLient = CommonItems.getClient();

        try {

            CartoDBResponse<Map<String, String>> res = cartoDBCLient.request(sql);

            for (Map<String, String> currentQuestion : res.getRows()) {
                response.add(new FetchResponseClass(
                        currentQuestion.get("name"),
                        currentQuestion.get("question_text"),
                        currentQuestion.get("question_id"),
                        currentQuestion.get("user_id"),
                        currentQuestion.get("date_time"),
                        currentQuestion.get("distance"),
                        currentQuestion.get("picture"),
                        currentQuestion.get("address"),
                        currentQuestion.get("answer_count"),
                        currentQuestion.get("profile_picture"),
                        currentQuestion.get("latitude"),
                        currentQuestion.get("longitude")));
            }

        } catch (CartoDBException e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(response, HttpStatus.OK);
    }


    @RequestMapping("/check_for_answers")
    public ResponseEntity<ServerResponse> check_for_answers(@RequestBody String input) {

        String userID;
        SecuredCartoDBClient cartoDBCLient = CommonItems.getClient();

        try {

            userID = input;

            String sql = "SELECT 1 from question_answers\n" +
                    "where question_id = (SELECT association_id " +
                    "from user_question_association where user_id = '" + userID + "')\n" +
                    "AND user_id NOT LIKE '" + userID + "'\n" +
                    "AND date_time > current_timestamp - interval '15 minutes'";

            CartoDBResponse<Map<String, String>> res = cartoDBCLient.request(sql);

            if (res.getTotal_rows() > 0) {
                return new ResponseEntity<>(new ServerResponse(String.valueOf(ANSWERS_PRESENT)), HttpStatus.OK);
            }

        } catch (CartoDBException e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(new ServerResponse(String.valueOf(ANSWERS_NOT_PRESENT)), HttpStatus.OK);
    }

    @RequestMapping(value = "/check_for_questions", method = RequestMethod.POST)
    public ResponseEntity<ServerResponse> check_for_questions(@RequestBody FetchRequestClass input) {

        double latitude;
        double longitude;
        String userID;

        if (input != null) {
            latitude = input.getLatitude();
            longitude = input.getLongitude();
            userID = input.getUserID();

            SecuredCartoDBClient cartoDBCLient = new SecuredCartoDBClient("josemarialanda",
                    "lolman007",
                    "PJ6Hms70JsBVc9ZBiRPfzTZ3pOKmYpl0j0FZDCtP",
                    "08slX3njzbQfudH1QKOacTVJ7c46oCWKKfYLSaig");

            try {

                String sql = "SELECT * FROM questions " +
                        "WHERE ST_Intersects(" +
                        "ST_Buffer(ST_GeomFromText" +
                        "('POINT(" + longitude + " " + latitude + ")', 4326)::geography, 1000)::geometry," +
                        "the_geom) " +
                        "AND user_id NOT LIKE '" + userID + "' " +
                        "AND date_time > current_timestamp - interval '15 minutes' " +
                        "AND " +
                        "( SELECT COUNT(*) " +
                        "FROM questions " +
                        "WHERE date_time > current_timestamp - interval '15 minutes' " +
                        ") >= 1 " +
                        "ORDER BY the_geom <-> " +
                        "ST_SetSRID(ST_MakePoint(" + longitude + "," + latitude + "),4326) " +
                        "ASC LIMIT 100";

                CartoDBResponse<Map<String, String>> res = cartoDBCLient.request(sql);

                if (res.getTotal_rows() > 0) {
                    return new ResponseEntity<>(new ServerResponse(String.valueOf(QUESTIONS_PRESENT)), HttpStatus.OK);
                }
            } catch (CartoDBException e) {
                e.printStackTrace();
            }
        }
        return new ResponseEntity<>(new ServerResponse(String.valueOf(QUESTIONS_NOT_PRESENT)), HttpStatus.OK);
    }


    /*
    @RequestMapping(value = "/check_for_nearby_questions", method = RequestMethod.POST)
    public ResponseEntity<ServerResponse> check_for_nearby_questions(@RequestBody FetchRequestClass input) {

        double latitude;
        double longitude;
        String userID;

        if (input != null) {
            latitude = input.getLatitude();
            longitude = input.getLongitude();
            userID = input.getUserID();

            SecuredCartoDBClient cartoDBCLient = new SecuredCartoDBClient("josemarialanda",
                    "lolman007",
                    "PJ6Hms70JsBVc9ZBiRPfzTZ3pOKmYpl0j0FZDCtP",
                    "08slX3njzbQfudH1QKOacTVJ7c46oCWKKfYLSaig");

            try {

                String sql = "SELECT * FROM questions " +
                        "WHERE ST_Intersects(" +
                        "ST_Buffer(ST_GeomFromText" +
                        "('POINT(" + longitude + " " + latitude + ")', 4326)::geography, 150)::geometry," +
                        "the_geom) " +
                        "AND user_id NOT LIKE '" + userID + "' " +
                        "AND date_time > current_timestamp - interval '15 minutes' " +
                        "AND " +
                        "( SELECT COUNT(*) " +
                        "FROM questions " +
                        "WHERE date_time > current_timestamp - interval '15 minutes' " +
                        ") >= 1 " +
                        "ORDER BY the_geom <-> " +
                        "ST_SetSRID(ST_MakePoint(" + longitude + "," + latitude + "),4326) " +
                        "ASC LIMIT 100";

                CartoDBResponse<Map<String, String>> res = cartoDBCLient.request(sql);

                if (res.getTotal_rows() > 0) {
                    return new ResponseEntity<>(new ServerResponse(String.valueOf(QUESTIONS_PRESENT)), HttpStatus.OK);
                }
            } catch (CartoDBException e) {
                e.printStackTrace();
            }
        }
        return new ResponseEntity<>(new ServerResponse(String.valueOf(QUESTIONS_NOT_PRESENT)), HttpStatus.OK);
    }
    */


    /*
    @RequestMapping(value = "/question_check", method = RequestMethod.POST)
    public void question_check(@RequestBody FetchRequestClass input) {

        double latitude;
        double longitude;
        String userID;

        if (input != null) {
            latitude = input.getLatitude();
            longitude = input.getLongitude();
            userID = input.getUserID();

            SecuredCartoDBClient cartoDBCLient = new SecuredCartoDBClient("josemarialanda",
                    "lolman007",
                    "PJ6Hms70JsBVc9ZBiRPfzTZ3pOKmYpl0j0FZDCtP",
                    "08slX3njzbQfudH1QKOacTVJ7c46oCWKKfYLSaig");

            try {

                String sql = "SELECT * FROM questions " +
                        "WHERE ST_Intersects(" +
                        "ST_Buffer(ST_GeomFromText" +
                        "('POINT(" + longitude + " " + latitude + ")', 4326)::geography, 1000)::geometry," +
                        "the_geom) " +
                        "AND user_id NOT LIKE '" + userID + "' " +
                        "AND date_time > current_timestamp - interval '15 minutes' " +
                        "AND " +
                        "( SELECT COUNT(*) " +
                        "FROM questions " +
                        "WHERE date_time > current_timestamp - interval '15 minutes' " +
                        ") >= 1 " +
                        "ORDER BY the_geom <-> " +
                        "ST_SetSRID(ST_MakePoint(" + longitude + "," + latitude + "),4326) " +
                        "ASC LIMIT 100";

                CartoDBResponse<Map<String, String>> res = cartoDBCLient.request(sql);

                if (res.getTotal_rows() > 0) {
                    new PushBuilder(null, userID, null, "QUESTION_CHECK").sendPush();
                } else {

                }
            } catch (CartoDBException e) {
                e.printStackTrace();
            }
        }
    }
    */
}